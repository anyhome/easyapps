avalon.filters.clearHtml =  function(str) { //用法： {{aaa}}
    str = str.replace(/<\/?[^>]*>/g,''); //去除HTML tag
    str = str.replace(/[ | ]*\n/g,'\n'); //去除行尾空白
    str = str.replace(/\n[\s| | ]*\r/g,'\n'); //去除多余空行
    str=str.replace(/&nbsp;/ig,'');//去掉&nbsp;
    return str;
}

require(["../../js/EasyApp"], function($) {
    var HomeCtl = avalon.define({
        $id: "HomeCtl",
        App:{},
        datalist:[],

        openContentWin:function(el){
            var url = "../Content/index.html?id="+el.id;
            $.openWin(url,'Content',{
                animation:{
                    type:'push',
                }
            });
        },

        run:function(){
            HomeCtl.App = $.App();
            $.getContentList(HomeCtl.App.id,0,function(req){
                HomeCtl.datalist = req['data']['volist'];
            });
        }
    })
    $(function(){
        HomeCtl.run();
        avalon.scan();
    })
})
