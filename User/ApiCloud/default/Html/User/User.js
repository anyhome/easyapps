var UserCtl = avalon.define({
    $id: "UserCtl",
    User:{},
    regMobile:'',
    mobile:'',
    account:'',
    name:'',
    nickname:'',
    password:'',
    repwd:'',
    waitsec : 90,
    waitTxt : '获取验证码',
    InterValObj : null,
    waitStatus:true,
    SetRemainTime:function(){
        if (UserCtl.waitsec < 2) {
            window.clearInterval(UserCtl.InterValObj);
        };
        UserCtl.waitsec --;
        UserCtl.waitStatus = false;
        UserCtl.waitTxt = UserCtl.waitsec+"秒以后重新获取";
        if (UserCtl.waitsec < 1) {
            UserCtl.waitTxt = "重新获取验证码";
            UserCtl.waitStatus = true;
            UserCtl.waitsec = 90;
        }
    },
    ckLogin:function(){
        if (!UserCtl.account || !UserCtl.password ) {
            $.alert('用户名或密码不能为空');
            return
        }
        var app = $.App();
        var url = $.ckLoginApi(app.id);
        var data = {
            account:UserCtl.account,
            password:md5(UserCtl.password),
        }
        $.post(url,data,function(req){
            if (req.ret == '0') {
                $.alert(req.msg);
            }else if (req.ret == '1') {
                $.alert(req.msg);
                $.setStorage('User',req['User'])
                $.redirect('index.html');
            };
        })
    },
    ckReg:function(){
        if (!UserCtl.account || !UserCtl.password || !UserCtl.repwd) {
            $.alert('用户名或密码不能为空');
            return
        }

        if (UserCtl.password != UserCtl.repwd) {
            $.alert('两次输入的密码不一致');
            return
        };
        var app = $.App();
        var url = $.ckRegApi(app.id);
        var data = {
            account:UserCtl.account,
            password:UserCtl.password,
            repwd:UserCtl.repwd,
        }
        $.post(url,data,function(req){
            if (req.ret == '0') {
                $.alert(req.msg);
            }else if (req.ret == '1') {
                $.alert(req.msg);
                $.redirect('login.html');
            };
        })
    },
    regSms:function(){
        var reg = /^0?1[3|4|5|8][0-9]\d{8}$/;
        if (!reg.test(UserCtl.regMobile)) {
            alert('请输入正确的手机号');
            return false;
        };
        UserCtl.InterValObj = window.setInterval(UserCtl.SetRemainTime, 1000);
    },
    run:function(){
        var app = $.App();
        UserCtl.User = $.User();
    }
})