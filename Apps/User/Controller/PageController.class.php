<?php
namespace User\Controller;
use Think\Controller;
class PageController extends CommonController {
    
    public function index($page = 1,$key = ''){
        cookie( '_currentUrl_', __SELF__ );
        if ( method_exists( $this, '_filter' ) ) {
            $map = $this->_filter();
        }
        $map['mid'] = $this->mid;
        $map['apid'] = $this->apid;
        $map['status'] = array('neq',-1);
        $model = D($this->con);
        $data = $model->getPage($map,$page,20,'idx asc');
        $this->assign('data', $data);
        $this->display();
    }

    public function updateField($value='',$pk = '',$name = '')
    {
        if (!$name || !$pk  ) return;
        // if ($name == 'icon') {
        //     $appath = 'User/'.numberDir($this->apid).'icon/';
        //     $appicon = $appath.$pk.'.png';
        //     if (!is_dir($appath)) {
        //         mkdir($appath,0777,true);
        //     }
        //     @copy($value, $appicon);
        //     $value = $appicon;
        // }

        // if ($name == 'picurl') {
        //     $appath = 'User/'.numberDir($this->apid).'appbg/';
        //     $appicon = $appath.$pk.'.png';
        //     if (!is_dir($appath)) {
        //         mkdir($appath,0777,true);
        //     }
        //     @copy($value, $appicon);
        //     $value = $appicon;
        // }

        $model = M( $this->con );
        $map['id'] = $pk;
        $map['mid'] = $this->mid;
        $data[$name] = $value;
        $model->where($map)->save($data);


        $this->success( '成功!');
    }

    public function delete($id='')
    {
        $Page = M('Page');
        $Content = M('Content');
        $map['id'] = $id;
        $map['mid'] = $this->mid;
        $Page->where($map)->delete();
        unset($map['id']);
        $map['pageid'] = $id;
        $Content->where($map)->delete();
        $this->success( '操作成功!');
    }

    public function show($id='')
    {
        $Content = M('Content');
        if (IS_POST) {
            $map['id'] = $id;
            $json = file_get_contents('php://input');
            $post = json_decode($json,true);
            $dd['json'] = serialize($post['json']);
            // $dd['json'] = I('json');
            $dd['content'] = $post['html'];
            $dd['title'] = $post['json']['title'];
            $dd['intro'] = $post['json']['desc'];
            $dd['picurl'] = $post['json']['cover'];
            $Content->where($map)->save($dd);
        }

        $vo = $Content->find($id);
        $data['code'] = 2;
        $data['message'] = 'Common:Updated';
        $data['data']['type'] = 2;
        $data['data']['show_id'] = $id;
        $data['data']['title'] = $vo['title'];
        $data['data']['desc'] = $vo['intro'];
        $data['data']['show_data_name'] = '837f9f4a370b497be02221bed60c543c';
        $data['data']['right_no_logo'] = 0;
        $data['data']['right_no_advert'] = 0;
        $data['data']['mask_no_modification'] = 0;
        $data['data']['mask_on_locking'] = 0;
        $data['data']['traffic'] = 0;
        $data['data']['points'] = 0;
        $data['data']['history_hits'] = 0;
        $data['data']['created_at'] = 0;
        $data['data']['updated_at'] = 0;
        $data['data']['show_data_url'] = U('Page/detail?id='.$id);
        // $data['data']['show_data_url'] = 'api/show/detail.jsonp';
        $data['data']['show_url'] = 'diaoyong.com/results_link';
        $this->ajaxReturn($data);
        
    }

    public function detail($id='')
    {
        // createTmpTpl();exit();
        $Content = M('Content');
        $vo = $Content->find($id);
        $dd = unserialize($vo['json']);
        $d = $dd['pages'][0];
        echo "tn_show_data_result(".json_encode($dd).")";
    }


    public function article()
    {
        //{"code":2,"message":"Common:Updated","data":{"show_id":2833383,"type":2,"version":1,"title":"fsdfd","desc":"fdsafdf","cover":"","show_data_name":"f7842efe3dffe5439a4359667c906dc0","right_no_logo":0,"right_no_advert":0,"mask_no_modification":0,"mask_on_locking":0,"traffic":0,"points":0,"history_hits":7,"created_at":"2015-07-29T04:55:01.000Z","updated_at":"2015-08-02T12:27:03.000Z","show_data_url":"http://statics.xiumi.us/xmi/ad/f7842efe3dffe5439a4359667c906dc0.json?_ver=1438518423000","show_url":"http://v.xiumi.us/board/v3/26eYK/2833383"}}
        //{"code":1,"message":"OK","data":{"fragment_id":44326,"fragment_url":"http://pb.ishangtong.com/getfragment.php?id=44326","created_at":"2015-07-29 12:53:29","updated_at":"2015-07-29 12:53:29"}}

        $json = file_get_contents('php://input');
        $tmj = json_decode($json,true);
        //tn_show_data_result
        $Content = M('Content');
        $data['mid'] = $this->mid;
        // $data['json'] = addslashes(json_encode($dd));
        $data['json'] = serialize($tmj);
        $data['appid'] = 1;
        $ret = $Content->add($data);
        if ($ret !== FALSE) {
            unset($data);
            $data['code'] = 1;
            $data['message'] ='OK';
            $data['data']['show_id'] = $ret;
            $data['data']['show_data_url'] ='';
            $data['data']['created_at'] =time();
            $data['data']['updated_at'] =time();
            $this->ajaxReturn($data);
        }else{
            $this->error('新增失败');
        }

    }
}