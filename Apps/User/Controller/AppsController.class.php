<?php
namespace User\Controller;
use Think\Controller;
class AppsController extends CommonController {
    

    public function updateField($value='',$pk = '',$name = '')
    {
        if (!$name || !$pk  ) return;

        if ($name == 'icon') {
            $appath = 'User/'.numberDir($this->apid).'icon/';
            $appicon = $appath.'icon.png';
            if (!is_dir($appath)) {
                mkdir($appath,0777,true);
            }
            @copy($value, $appicon);
            $value = $appicon;
        }

        if ($name == 'picurl') {
            $appath = 'User/'.numberDir($this->apid).'appbg/';
            $appicon = $appath.'cover.png';
            if (!is_dir($appath)) {
                mkdir($appath,0777,true);
            }
            @copy($value, $appicon);
            $value = $appicon;
        }


        $model = M( $this->con );
        $map['id'] = $pk;
        $map['mid'] = $this->mid;
        $data[$name] = $value;
        $model->where($map)->save($data);


        $this->success( '成功!');
    }
}