<?php
namespace User\Controller;
use Think\Controller;
class IndexController extends CommonController {
    public function index(){
        $Apps = M('Apps');
        $map['mid'] = $this->mid;
        $apps = $Apps->where($map)->select();
        $this->assign('apps', $apps);
        $this->display();
    }
    
}