<?php
namespace Api\Controller;
use Think\Controller;
class PageController extends CommonController {
    public function index($pageid = 0,$page = 1){
        $data = array();

        $Content = D('Content');
        $map['apid'] = $this->apid;
        $map['pageid'] = $pageid;
        $data = $Content->getPage($map,$page,14);

        unset($map);
        $Page = D('Page');
        $map['apid'] = $this->apid;
        $map['id'] = $pageid;
        $data['pageInfo'] = $Page->where($map)->find();

        $this->ajaxReturn($data,$this->format);
    }

    public function getAllPages(){
        $Page = D('Page');
        $data['apid'] = $this->apid;
        $data['data'] = $Page->where($map)->select();
        $this->ajaxReturn($data,$this->format);
    }
}
